var updatebtns = document.getElementsByClassName("update-btn");
for(i = 0; i < updatebtns.length; i ++){
    updatebtns[i].addEventListener("click", function () {
        var productId = this.dataset.product
        var action = this.dataset.action
        addCookieItem(productId, action);
    })
}

function addCookieItem(productId, action) {
    if(action === "add"){
        if(cart[productId] ===undefined){
            cart[productId] = {"quantity": 1};
        }else {
            cart[productId]["quantity"] += 1;
        }

    }
    if(action === "remove"){
        console.log("some")
        cart[productId]["quantity"] -=1
        if(cart[productId]["quantity"] <= 0){
            delete cart[productId]
        }

    }
    document.cookie = "cart=" + JSON.stringify(cart) + ";domain=;path=/"
    location.reload()
}

function updateOrder(productId, action) {
    var url = "/update_item/"

    fetch(url, {
        method: "POST",
        headers:{
            "Context-Type": "application/json",
            "X-CSRFToken":csrfToken,
        },
        body: JSON.stringify({"productId": productId, "action": action})

    })
        .then((response)=>{
            return response.json()
        })
        .then((data)=>{
            location.reload()
        })
}