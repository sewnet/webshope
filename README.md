# Webstore

## Technologies used

Internally the server is built with

- Python 3.7.x
- Django with latest LTS version, currently v3.2.11
- DRF (Django REST Framework) with latest version matching Django version



## Development setup

Clone the repository from bitbucket

    git clone https://sewnet@bitbucket.org/sewnet/webshope.git


Install and activate the virtual environment:

    pip3 install virtualenv
    virtualenv -p python3 venv
    source venv/bin/activate


Install the required Python packages:

    pip install -r requirements.txt

Create the Django database with

    python manage.py migrate

Create a new admin with

    python manage.py createsuperuser

Create data base load with test data

    python manage.py loaddata ecommerce/fixtures/db.json

Run the development server (will reload itself automatically when code changes)

    python manage.py runserver

Now you should be able to access the application and admin view

    http://localhost:8000

Happy hacking!