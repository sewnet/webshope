import os
from pathlib import Path

from django.shortcuts import render

from .models import *
from .utils import cart_handler

BASE_DIR = Path(__file__).resolve().parent.parent


def product_list(request):
    cart = cart_handler(request)
    products = Product.objects.all()
    if request.method == 'POST':
        query = request.POST.get('search')
        products = Product.objects.filter(name__icontains=query)

    context = {"products": products, "items": cart["items"], "order": cart["order"], "cart_items": cart["cart_items"]}
    return render(request, "productList.html", context=context)


def product_detail(request, id):
    cart = cart_handler(request)
    product = Product.objects.get(id=id)
    context = {"product": product, "items": cart["items"], "order": cart["order"], "cart_items": cart["cart_items"]}
    return render(request, "productPage.html", context=context)


def shopping_cart(request):
    cart = cart_handler(request)

    context = {"items": cart["items"], "order": cart["order"], "cart_items": cart["cart_items"]}

    return render(request, "cart.html", context=context)


def checkout(request):
    cart = cart_handler(request)

    context = {"items": cart["items"], "order": cart["order"], "cart_items": cart["cart_items"]}
    return render(request, "checkout.html", context=context)


def manage_products(request):
    cart = cart_handler(request)
    products = Product.objects.all()
    context = {"products": products, "items": cart["items"], "order": cart["order"], "cart_items": cart["cart_items"]}
    if request.method == 'POST':
        name = request.POST.get('Name')
        price = request.POST.get('Price')
        barcode = request.POST.get('Barcode')
        file_item = request.FILES.get('Image')
        description = request.POST.get('description')
        if file_item:
            fn = os.path.join(BASE_DIR, 'static/images/{}'.format(file_item))
            open(fn, 'wb').write(file_item.file.read())
        Product.objects.create(name=name, price=price, barcode=barcode, image=file_item, description=description)
        return render(request, "productList.html", context=context)
    return render(request, "manageProducts.html", context=context)


def edit_product(request, id):
    cart = cart_handler(request)
    product = Product.objects.get(id=id)
    context = {"product": product, "items": cart["items"], "order": cart["order"], "cart_items": cart["cart_items"]}
    if request.method == 'POST':
        product.name = request.POST.get('Name')
        product.price = request.POST.get('Price')
        product.barcode = request.POST.get('Barcode')
        product.image = request.FILES.get('Image')
        file_item = request.FILES.get('Image')
        if file_item:
            fn = os.path.join(BASE_DIR, 'static/images/{}'.format(file_item))
            open(fn, 'wb').write(file_item.file.read())
        product.description = request.POST.get('description')

        product.save()
        context = {"product": product, "items": cart["items"], "order": cart["order"], "cart_items": cart["cart_items"]}
        return render(request, "productPage.html", context=context)
    return render(request, "editProduct.html", context=context)


def delete_product(request, id):
    cart = cart_handler(request)
    products = Product.objects.all()
    context = {"products": products, "items": cart["items"], "order": cart["order"], "cart_items": cart["cart_items"]}
    try:
        Product.objects.get(id=id).delete()
    except Product.DoesNotExist:
        pass
    return render(request, "manageProducts.html", context=context)


def search_list(request):
    cart = cart_handler(request)
    products = {}
    query = ""
    if request.method == 'POST':
        query = request.POST.get('search')
        products = Product.objects.filter(name__icontains=query)

        if products.count() == 0:
            products = Product.objects.filter(barcode__icontains=query)

    context = {"products": products, "items": cart["items"], "order": cart["order"], "cart_items": cart["cart_items"],
               "search": query}
    return render(request, "productList.html", context=context)
