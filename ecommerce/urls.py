from django.urls import path

from . import views

urlpatterns = [
    path('', views.product_list, name='productList'),
    path('<int:id>/detail/', views.product_detail, name='productPage'),
    path('cart/', views.shopping_cart, name='cart'),
    path('checkout/', views.checkout, name='checkout'),
    path('manage-products/', views.manage_products, name='manageProducts'),
    path('search/', views.search_list, name='search'),
    path('<int:id>/edit/', views.edit_product, name='edit'),
    path('<int:id>/delete/', views.delete_product, name='delete'),
]
