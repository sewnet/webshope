import json

from .models import *


def cart_handler(request):
    try:
        cart = json.loads(request.COOKIES["cart"])
    except:
        cart = {}

    items = []
    order = {"get_cart_total": 0, "get_cart_items": 0, "shipping": False}
    cart_item = order["get_cart_items"]

    for i in cart:
        cart_item = cart[i]["quantity"]

        cart_product = Product.objects.get(id=i)
        total = (cart_product.price * cart[i]["quantity"])

        order["get_cart_total"] += total
        order["get_cart_items"] += cart[i]["quantity"]

        item = {
            "product": {
                "id": cart_product.id,
                "name": cart_product.name,
                "price": cart_product.price,
                "image_url": cart_product.image_url

            },
            "quantity": cart[i]["quantity"],
            "get_total": total
        }
        items.append(item)

    return {"items": items, "order": order, "cart_items": cart_item}
